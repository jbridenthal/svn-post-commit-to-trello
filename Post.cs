﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;
using SVN_Post_Commit.Serialization_Classes;

namespace SVN_Post_Commit
{
    internal class Post
    {
        private readonly Dictionary<string, string> _checklists = new Dictionary<string, string>();
        private readonly Settings _settings;
        private string _checklistId;
        private string _jsonReturn;

        public Post()
        {
            _settings = Debugger.IsAttached ? new Settings(true) : new Settings(false);
        }

        public bool PostToCard(string cardShortId, string user, string checkInDate,
                               string checkInMesssage, string transaction)
        {
            bool success = false;
            GetJsonStringData();
            ParseChecklistsIntoDictionary();
            if (!CardHasChanglistAlready(cardShortId))
                success = AddNewChangelist(cardShortId);
            try
            {
                PostNewItemToChangeList(user, checkInDate, checkInMesssage, transaction);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        private void GetJsonStringData()
        {
            string link = "";
            link +=
                String.Format(
                    @"https://api.trello.com/1/boards/{0}?checklists=all&cards=all&key={1}&token={2}",
                    _settings.Board, _settings.Key, _settings.Token);
            var wc = new WebClient();
            _jsonReturn = wc.DownloadString(link);
        }

        private TrelloRootObject ParseTrelloRoot()
        {
            var trelloRoot = JsonConvert.DeserializeObject<TrelloRootObject>(_jsonReturn);
            return trelloRoot;
        }

        private void ParseChecklistsIntoDictionary()
        {
            _checklists.Clear();
            TrelloRootObject trelloRoot = ParseTrelloRoot();
            foreach (TrelloChecklist trelloChecklist in trelloRoot.checklists)
            {
                _checklists.Add(trelloChecklist.id, trelloChecklist.name);
            }
        }

        private bool CardHasChanglistAlready(string cardShortID)
        {
            TrelloRootObject trelloRoot = ParseTrelloRoot();
            bool hasChangelist = false;
            foreach (TrelloCard card in trelloRoot.cards)
            {
                if (card.idShort == Convert.ToInt32(cardShortID))
                {
                    hasChangelist = LookForChangelist(card);
                    break;
                }
            }

            return hasChangelist;
        }

        private bool LookForChangelist(TrelloCard card)
        {
            bool foundChangelist = false;
            foreach (string checklistID in card.idChecklists)
            {
                if (_checklists.ContainsKey(checklistID) && _checklists[checklistID].Equals("Changelists"))
                {
                    foundChangelist = true;
                    _checklistId = checklistID;
                    break;
                }
            }
            return foundChangelist;
        }

        private bool AddNewChangelist(string cardShortId)
        {
            bool success;
            try
            {
                string cardLongId = GetCardIdFromShortId(cardShortId);
                string checklistId = GetNewChangelistId();
                AddNewChangelistToCard(cardLongId, checklistId);
                success = true;
            }
            catch
            {
                success = false;
            }
            return success;
        }

        private void AddNewChangelistToCard(string cardLongId, string checklistId)
        {
            string url = String.Format(@"https://trello.com/1/cards/{0}/checklists", cardLongId);
            string trelloParameters = String.Format("?value={0}&key={1}&token={2}", checklistId, _settings.Key,
                                                    _settings.Token);
            using (var wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                wc.UploadString(url + trelloParameters, "POST");
            }
        }

        private string GetCardIdFromShortId(string cardShortId)
        {
            string cardLongId = null;
            TrelloRootObject trelloRoot = ParseTrelloRoot();
            foreach (TrelloCard trelloCard in trelloRoot.cards)
            {
                if (trelloCard.idShort == Convert.ToInt32(cardShortId))
                {
                    cardLongId = trelloCard.id;
                    break;
                }
            }

            return cardLongId;
        }

        private string GetNewChangelistId()
        {
            string checklistId;
            string trelloParameters = String.Format("?name=Changelists&idBoard={0}&key={1}&token={2}", _settings.Board,
                                                    _settings.Key, _settings.Token);
            const string url = @"https://trello.com/1/checklists";
            using (var wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string returnedChecklistJson = wc.UploadString(url + trelloParameters, "POST");
                var checklists = JsonConvert.DeserializeObject<TrelloChecklist>(returnedChecklistJson);
                checklistId = checklists.id;
                _checklistId = checklists.id;
            }
            return checklistId;
        }

        private void PostNewItemToChangeList(string user, string checkInDate,
                                             string checkInMesssage, string transaction)
        {
            string url = String.Format(@"https://trello.com/1/checklists/{0}/checkItems", _checklistId);
            checkInMesssage = checkInMesssage.Replace(" ", "%20");
            string changelistInformation = transaction + "%20" + checkInMesssage + "%20" + user + "%20" + checkInDate;
            string trelloParameters = String.Format("?name={0}&key={1}&token={2}", changelistInformation, _settings.Key,
                                                    _settings.Token);
            using (var wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                wc.UploadString(url + trelloParameters, "POST");
            }
        }
    }
}