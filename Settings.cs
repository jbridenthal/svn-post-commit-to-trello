﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using SVN_Post_Commit.Serialization_Classes;

namespace SVN_Post_Commit
{
    internal class Settings
    {
        private readonly bool _debug;

        public Settings(bool debug)
        {
            _debug = debug;
            GetSettings();
        }

        public string Key { get; private set; }
        public string Token { get; private set; }
        public string Board { get; private set; }

        private void GetSettings()
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\settings.xml";
            var serializer = new XmlSerializer(typeof (root));
            using (var reader = new StreamReader(path))
            {
                var settingsRoot = (root) serializer.Deserialize(reader);
                foreach (rootSettings rSettings in settingsRoot.Items.Where(rSettings => rSettings.debug == _debug))
                {
                    Board = StripNewLine(rSettings.board);
                    Key = StripNewLine(rSettings.key);
                    Token = StripNewLine(rSettings.token);
                }
            }
        }

        private string StripNewLine(string value)
        {
            value = value.Replace("\n", " ");
            return value.Trim();
        }
    }
}