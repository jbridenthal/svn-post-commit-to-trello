﻿using System;
using System.Diagnostics;

namespace SVN_Post_Commit
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string transaction;
            string user = "";
            string checkInDate = "";
            string checkInMessage = "";
            string cardID = "";


            if (!Debugger.IsAttached)
            {
                string repositories = args[0];
                transaction = args[1];
                ProcessStartInfo processStartInfo = GetProcessStartInfo(repositories, transaction);
                using (Process svnlook = Process.Start(processStartInfo))
                {
                    string svnlookReturnMessage = svnlook.StandardOutput.ReadToEnd();
                    string[] messageSplit = svnlookReturnMessage.Split('\r');
                    if (messageSplit[3].TrimStart().StartsWith("Trello:"))
                    {
                        user = messageSplit[0];
                        checkInDate = messageSplit[1].Substring(0, 20);
                        messageSplit = messageSplit[3].Split(':');
                        messageSplit = messageSplit[1].TrimStart().Split(' ');
                        cardID = messageSplit[0];
                        for (int i = 1; i <= messageSplit.GetUpperBound(0); i++)
                        {
                            checkInMessage += messageSplit[i] + " ";
                        }
                        var post = new Post();
                        bool test = post.PostToCard(cardID, user, checkInDate, checkInMessage, transaction);
                    }
                }
            }
            else
            {
                transaction = "74";
                user = Environment.UserName;
                checkInDate = DateTime.Now.ToString();
                checkInMessage = "This is a debugger test";
                cardID = "9";
                var post = new Post();

                bool test = post.PostToCard(cardID, user, checkInDate, checkInMessage, transaction);
            }
        }

        private static ProcessStartInfo GetProcessStartInfo(string repositories, string transaction)
        {
            var processStartInfo = new ProcessStartInfo
                                       {
                                           FileName = "svnlook.exe",
                                           UseShellExecute = false,
                                           CreateNoWindow = true,
                                           RedirectStandardOutput = true,
                                           RedirectStandardError = true,
                                           Arguments =
                                               String.Format("info -r \"{0}\" \"{1}\"", transaction, repositories)
                                       };
            return processStartInfo;
        }
    }
}