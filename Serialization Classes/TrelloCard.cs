﻿using System.Collections.Generic;

namespace SVN_Post_Commit.Serialization_Classes
{
    internal class TrelloCard
    {
        public string id { get; set; }
        public List<string> idChecklists { get; set; }
        public int idShort { get; set; }
    }
}