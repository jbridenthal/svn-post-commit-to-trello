﻿namespace SVN_Post_Commit.Serialization_Classes
{
    internal class TrelloPrefs
    {
        public string permissionLevel { get; set; }
        public string voting { get; set; }
        public string comments { get; set; }
        public string invitations { get; set; }
        public bool selfJoin { get; set; }
        public bool cardCovers { get; set; }
    }
}