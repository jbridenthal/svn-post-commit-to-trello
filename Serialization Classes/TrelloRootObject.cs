﻿using System.Collections.Generic;

namespace SVN_Post_Commit.Serialization_Classes
{
    internal class TrelloRootObject
    {
        public string id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public bool closed { get; set; }
        public bool pinned { get; set; }
        public string url { get; set; }
        public TrelloPrefs prefs { get; set; }
        public List<TrelloChecklist> checklists { get; set; }
        public List<TrelloCard> cards { get; set; }
    }
}