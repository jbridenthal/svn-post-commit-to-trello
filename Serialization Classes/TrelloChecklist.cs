﻿using System.Collections.Generic;

namespace SVN_Post_Commit.Serialization_Classes
{
    internal class TrelloChecklist
    {
        public string id { get; set; }
        public string name { get; set; }
        public string idBoard { get; set; }
        public List<object> checkItems { get; set; }
    }
}